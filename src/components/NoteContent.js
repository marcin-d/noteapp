import React, { Component } from 'react'

class NoteContent extends Component {
  constructor(props) {
    super(props)

    this.state = {
      note: this.props.note
    }
  }

  TitleHandler(event){
    let currentState = this.state.note
    currentState.title = event.target.value
    this.setState({
      note: currentState
    })
  }

  ContentHandler(event){
    let currentState = this.state.note
    currentState.content = event.target.value
    this.setState({
      note: currentState
    })
  }

  render() {
    const {title, content} = this.state.note
    return (
      <article>
        <div className='NoteContent'>
          <input onChange={(event) => this.TitleHandler(event)} className='title-edit' type='text' defaultValue={title} placeholder='Your Title Here' maxLength="30"></input>
          <textarea onChange={(event) => this.ContentHandler(event)} className='content' defaultValue={content} maxLength="2800" placeholder='Your Note Here'></textarea>
          <div className='noteColor'>
            Current Color: <div id={this.state.note.color} className='currentColor'></div>
          </div>
        </div>
      </article>
    )
  }
}

export default NoteContent
