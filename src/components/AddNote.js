import React, { Component } from 'react'
import { cookiesContext } from './ReadCookiesC'

class AddNote extends Component {
  constructor(props) {
    super(props)

    this.state = {
    }

    this.addNew = this.addNew.bind(this)
  }


  addNew(LastID) {

    let dataNow = new Date()
    let year = dataNow.getFullYear()
    let month = dataNow.getMonth()
    let day = dataNow.getDay()
    let hours = dataNow.getHours()
    let minutes = dataNow.getMinutes()
    let seconds = dataNow.getSeconds()
    let date = `${year}.${month}.${day} ${hours}:${minutes}:${seconds}`


    document.cookie = `LastID=${LastID}`
    document.cookie = `Note${LastID}={"title":"Your Title", "content":"Wirte your note here", "id":"${LastID}", "color":"blue", "date": "${date}"}`
    let newNote = `Note${LastID}={"title":"Your Title", "content":"Wirte your note here", "id":"${LastID}", "color":"blue", "date": "${date}"}`
    return newNote
  }

  render(){
    return (
      <div className='sidebar-header'>
      <h1>Your Notes</h1>
      <cookiesContext.Consumer>
        {({LastID,AddNotec}) =>(
          <i className='fa fa-plus-square icon-plus' onClick={() => AddNotec(this.addNew(LastID))}></i>
        )}
      </cookiesContext.Consumer>
    </div>
  )
}
}

export default AddNote

