import React, { Component } from 'react'
import { cookiesContext } from './ReadCookiesC'

class Notes extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notes: this.props.notes.notes
    }
  }

  render() {
    //Creating list of notes
    let ToDisplay = this.state.notes.map((note) =>{
      return(
        <cookiesContext.Consumer key={note.id}>
          {({DisplayNote}) => (
            <div className='singleNote' onClick={(e) => DisplayNote(note)}>
              <h1 className='title'>{note.title}</h1>
              <p className='date'>{note.date}</p>
              <div className='currentColor s-color' id={note.color}></div>
            </div>
          )
          }
        </cookiesContext.Consumer>
      )
    })
    return (
        <aside>
          <div className='notes'>
            {ToDisplay}
          </div>
        </aside>
    )
  }
}

export default Notes