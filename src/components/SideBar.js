import React, { Component } from 'react'
import Notes from './Notes'
import AddNote from './AddNote'


class SideBar extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notes: this.props.notes
    }
  }

  render() {
    return (
      <div className='sidebar'>
          <AddNote/>
          <Notes notes={this.state.notes}/>
      </div>
    )
  }
}

export default SideBar
