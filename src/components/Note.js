import React, { Component } from 'react'
import NoteContent from './NoteContent'

class Note extends Component {
  constructor(props) {
    super(props)
    this.state = {
      note: this.props.content
    }
  }

  changeColor(color){
    let currentState = this.state.note
    currentState.color = color
    this.setState({
      note: currentState
    })
  }

  save(){
    const {title, content, id, color} = this.state.note

    let dataNow = new Date()
    let year = dataNow.getFullYear()
    let month = dataNow.getMonth()
    let day = dataNow.getDay()
    let hours = dataNow.getHours()
    let minutes = dataNow.getMinutes()
    let seconds = dataNow.getSeconds()
    let date = `${year}.${month}.${day} ${hours}:${minutes}:${seconds}`

    document.cookie = `Note${id}={"title":"${title}", "content":"${content}", "id":"${id}", "color":"${color}", "date": "${date}"}`
    this.props.save()
  }

  Delete(id){
    console.log(id)
    this.props.Delete(id)
  }

  render() {
    return(
      <section className="NoteView">
        <div className="NoteView-d">
            <menu>
              <div className='NoteOptions'>
                  <div className='changeColor'>
                  <button onClick={(e)=> this.changeColor('green')} id='green' className='color'></button>
                  <button onClick={(e) => this.changeColor('red')} id='red' className='color'></button>
                  <button onClick={(e) => this.changeColor('blue')} id='blue' className='color'></button>
                  </div>
                  <i className='fas fa-trash icon-delete' onClick={(e) => this.Delete(this.state.note.id)}></i>
              </div>
            </menu>
            <NoteContent note={this.state.note}/>
            <button onClick={(e) => this.save()} className='save'>Save</button>
        </div>
      </section>
    )
  }
}

export default Note