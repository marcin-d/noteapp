import React, { Component } from 'react';
import './App.css';
import SideBar from './components/SideBar';
import Note from './components/Note'
import { cookiesContext } from './components/ReadCookiesC'

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      notes: [],
      LastID: 0,
      Display: undefined,
      AddNotec: this.AddNotec,
      DisplayNote: this.DisplayNote
    }

    this.save = this.save.bind(this)
  }


  componentWillMount() {
    let MyCookies = document.cookie
    let OtherData = []
    let dataFromCookies = MyCookies.split(';')
    if (dataFromCookies.length > 1) {
      for (let data of dataFromCookies) {
        let note = data.slice(data.indexOf('=') + 1)
        if (note !== ''){
          OtherData.push(JSON.parse(note))
        }
      }
    }
    for (let data of OtherData) {
      if (typeof (data) == 'number') {
        this.setState({
          LastID: data + 1
        })
      } else {
        this.setState(prevState => {
          {
            notes: prevState.notes.push(data)
          }
        })
      }
    }

  }


  AddNotec = data =>{
    let NewNotes = this.state.notes
    let ReadyData = data.slice(data.indexOf('=') + 1)
    ReadyData = JSON.parse(ReadyData)
    NewNotes.push(ReadyData)
    this.setState(({
      notes: NewNotes,
      LastID: this.state.LastID + 1,
      Display: ReadyData
    }))
  }

  DisplayNote = data =>{
    this.setState({
      Display: data
    })
  }

  save(){
    this.setState({
      Display: undefined
    })
  }

  Delete(id){
    let newState = this.state.notes

    for (let obj in newState){
      if (newState[obj].id === id){
        newState.splice(obj,1)
      }
    }

    this.setState({
      notes: newState,
      Display: undefined
    })

    document.cookie = `Note${id}=`

  }

  render() {
    return (
      <main>
        <cookiesContext.Provider value={this.state}>
          <div className="App">
              <SideBar notes={this.state}/>
              {
                this.state.Display === undefined ?(
                  <div className='emptyScreen'></div>
                ):(
                    <Note content={this.state.Display} save={this.save} Delete={(id) => this.Delete(id)}/>
                )
              }
          </div>
        </cookiesContext.Provider>
      </main>
    );
  }
}

export default App;
